## Installation Guide

```sh
git clone git@bitbucket.org:cj12mid/doxy-docs.git
```
```js
var { Parser, MarkdownHelper, HTMLHelper } = require("*/doxy-docs");
```

# 🖥️ Platform supported
| Linux       | Mac OS | Windows     |
| ----------- | ------ | ----------- |
|  ✅  | ✅ |  ✅ |
  
<hr>

# 🤔 What is this?
doxy-docs can parse doxygen's xml directory and output files in html or markdown format for customizing documents.
But it is not very stable for the time being, so it takes time to maintain and it is not recommended to apply it in a large-scale production environment.

# ✨ Features
- Open the source code
- More efficient parser
- Refactor using c++
- Support custom HTML and CSS
- More and more

## 📊 Doxygen checked versions

| **version**                                   | ⚡ status ⚡ 
| ------------------------------------------ | ---------- |
| Release 1.8.16                   | ✅          |
| Release 1.8.15                       | ✅          |
| Release 1.8.14    | ✅          | 
| Release 1.8.13                            | ✅          |
| Release 1.8.12                            | ✅          |
| Release 1.8.11 | ✅          | 

# 🎬 Getting Started
```js
var { Parser, MarkdownHelper, HTMLHelper, languageMap } = require("*/doxy-docs");

// set the language.
languageMap.set("en", {
    kindHeader：function(pluralizeKind: string) { return `This is a list of all ${pluralizeKind}.`; },
    kindList：function(kind: string) { return `${kind} Lists`; },
    declared：function(fileDetail: string) { return `Declared in \`${fileDetail}\``; }
})

var parser = new Parser({
    prefixPath: "<FOLDER_URL>",
    onRequireXML: function(xmlPath, __IS_NODE_ENV__) {
        /**
         * need return xml content.
         */
        return "XML_CONTENT";
    },
    replaceName: "DOXYGEN_COMPOUND_NAME",
    aliasKinds: [{ kind: "namespace", aliasKind: "package" }],
    sectionFilter: function(kind) {
        /**
         * this is doxygen member kind.
         */
        return kind !== "private";
    },
    excludeCompoundKinds: ["file", "dir"],
    /**
     * use this func replace compound name.
     * @params
     * params.name: string;
     * params.kind: string;
     */
    renderInnerName: function (params) {
        return params.name
    },
    removePrefixCompound: function(name, kind) {
        return name.replace("MY_CUSTOM_NAMESPACE", "NEW_TEXT")
    },
    /**
     * set language.
     */
    language: "en" || "zh" || "ru" || "fr"
});
```

# 🎪 Contributors
doxy-docs was invented by [cj12mid](https://bitbucket.org/cj12mid/) in 2017. For my own benefit, this project only provides compressed code for now.


# 🧤 Licenses
We don't need licenses, everything is free, and we also do not provide services 🥴.